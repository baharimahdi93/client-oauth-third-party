<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});


Route::get('/redirect', function () {
    $query = http_build_query([
        'client_id' => '1',
        'redirect_uri' => 'http://localhost:8000/callback',
        'response_type' => 'code',
        'scope' => '',
    ]);

    return redirect('http://mahdi.com/oauth/authorize?'.$query);
});

Route::get('/callback', function (Request $request) {
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://mahdi.com/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => '1',
            'client_secret' => 'UICV2Oc1CUAHVXQkuzHvdKcI8jr0Nx9OJAyN3EyV',
            'redirect_uri' => 'http://localhost:8000/callback',
            'code' => $request->code,
        ],
    ]);

    return json_decode((string) $response->getBody(), true);
});
Route::get('/reset-token',function (){
    $http = new GuzzleHttp\Client;

    $response = $http->post('http://mahdi.com/oauth/token', [
        'form_params' => [
            'grant_type' => 'refresh_token',
            'refresh_token' => 'def502004d1ea4de5b79b7e34fce17b76239739d38ddf0ec1db93ed126920ad470012ee812a28f82e8bdb78788a8af0fb4aa22f8062a4579dfa9fc08acf114665952a5a85304386cb5bb9065cab0b61bf2333d3473af2072e975bad9e99ce0f36e259ea27bcd1c08b15749f311f223f2daca54dd41b082fd6032a4f385fb41f347571cef35545535573a1ec69f4f067900b88d1d12a40ae101278a9ca19030155602ae6c5e20e8df4625c76c6ff2491211dcecbda27a0d2e6404233c5b41a2018e1063ac7f7aeee6847418f70ef927223a535fcba5f66b6c75904c0940c2ce0dba7775ef80146419ad4960d7a2533735e876e64eb70ca990728a5a55cede6885e4ea38ea80951fe18c6543ad01cd469787c74bdcf9765255c6a55a41eb6819122acfb1dfa356c1905b7dcae2425396d2035eefed0b03c0bbe5116b52b52662f4f9ba45ff179ff525b6a541a6907607591ed097ca3b2825b80896b0ac6c7f41fb55',
            'client_id' => '1',
            'client_secret' => 'UICV2Oc1CUAHVXQkuzHvdKcI8jr0Nx9OJAyN3EyV',
            'scope' => '',
        ],
    ]);

    return json_decode((string) $response->getBody(), true);
});
